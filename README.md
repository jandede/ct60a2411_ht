<<<<<<< HEAD
﻿TIMO
=======
TIMO
>>>>>>> 19ad9e6101a4dc7890c94844b5c39e9788d27277

Java-ohjelma, jolla voidaan luoda kuviteellisia paketteja ja lähettää niitä SmartPost-automaatista toiseen.

Ohjelma tehtiin kurssin CT60A2411 Olio-ohjelmointi harjoitustyöksi. Ohjelma on kirjoitettu pääosin Java-kielellä, jonka lisäksi on käytetty Googlen tarjoamaa Javascript API:a kartan luomiseen ja siihen piirtämistä varten. Postin API sen sijaan on XML-muodossa, joka luetaan ja prosessoidaan Javalla.

Ohjelman vaatimukset:
Ohjelman ajamiseen vaaditaan Java 7 ja Internet-yhteys.

Ohjelman käyttö:
Ohjelman pääikkunassa on kartta sekä JavaFX-komponentteja. Karttaa pystyy selaamaan hiirellä raahaamalla. Mikäli lokitiedosto on olemassa ohjelman aloitusajankohtana, pystyy käyttäjä lataamaan sen, jolloin "Pakettien lähetys" osiossa olevaan ComboBox-komponenttiin tulee kaikki lokitiedostossa olevat paketit. Paketin valitsemisen jälkeen voi lähettää paketin, jolloin kartalle piirtyy sen reitti. Piirretyt reitit voidaan myös poistaa kartalta tyhjennysnapista.
Kartalle voi myös piirtää SmartPost automaatteja valitsemalla vasemman puoleisesta ComboBox-komponentista halutun kaupungin, jolloin kyseisen kaupungin kaikki automaatit merkitään kartalle. Kun merkittyä automaattia klikataan kartalla, aukeaa sen ylle lisäinformaatota, joka kertoo automaatin nimen ja aukioloajat. 
Uuden paketin voi luoda painamalla "Uusi paketti" -nappia, jolloin ruudulle aukeaa uusi ikkuna. Tästä ikkunasta käyttäjä voi antaa paketille nimen, mitat, painon, sekä lähtöpaikan ja määränpään. Käyttäjällä on myös mahdollisuus merkitä paketti särkyväksi. Tämän lisäksi käyttäjä joutuu valitsemaan paketin luokan(kuten tehtävänannossa mainittiin), mutta ohjelman logiikka määrää silti, missä luokassa paketti lähetetään. Kaikista virheistä ilmoitetaan käyttäjälle, jotta tämä osaa korjata mahdolliset kirjoitus- tai valintavirheet. Ohjelma myös kertoo mikäli paketti ei ole lähetettävissä(jos se ei sovellu mihinkään pakettiluokkaan). Tässä ikkunassa on myös nappi, josta painettuaan käyttäjä pääsee lukemaan kaikille pakettiluokille ominaiset rajoitteet, jotta tämä osaa valita pakettiluokan oikein.
Paketin luotuaan, käyttäjä joutuu painamaan "Päivitä pakettiluettelo" -nappia, jotta paketit näkyvät ComboBox-komponentissa.
Pääikkunassa on myös nappi, joka avaa uuden ikkunan pakettitaulukolle. Pakettitaulukossa näkyy järjestelmällisesti kaikkien pakettien tarkemmat tiedot, kuten mitat, paino, luokka, automaatit ja niiden etäisyys toisistaan.

Ohjelman koodi:
Ohjelman koodi koostuu 14 Java-lähdekooditiedostosta(ts. Java class -tiedosto), neljästä FXML-tiedostosta ja yhdestä CSS-tiedostosta. Kaikille ohjelman luokille on tehty omat Java-tiedostot selkeyden vuoksi, mukaanlukien FXML-tiedostoille paritetuista ohjaimista(*Controller.java). FXML-tiedostot sisältävät merkintäkoodia, eli ne määrää kunkin dialogin komponenttien attribuutit. CSS-tiedosto asettaa tyylit komponenteille ja ikkunoille.

Java-lähdekooditiedostot:

DataBuilder.java:
Lataa ja prosessoi Postin sivuilta löytyvän XML-tiedoston. Tiedostosta löytyy eri SmartPost-automaattien tiedot, joita ohjelmassa tarvitaan lähes kaikkeen. Tässä luokassa ollaan käytetty ns. singleton-suunnitteluperiaatetta, eli luokkaa on ohjelmassa vain yksi instanssi, koska DataBuilder ei ole tyypillinen objekti, jota pitäisi olla samanaikaisesti useampi. XML:n parsittua, luokka tallentaa tiedot ArrayList-muuttujaan, jota muut luokat voivat käyttää kutsuttuaan DataBuilderin getSmartPost-metodia.

DialogController.java:
Ohjain Lisää paketti -ikkunalle(Dialog.fxml). Luokka lukee käyttäjän antamat syötteet(lähetettävän paketin tiedot), jonka jälkeen ohjelman logiikka tarkastaa syötteet, ja joko lähettää paketin(luo Package-objektin) tai ilmoittaa käyttäjälle virheestä. Virheitä aiheuttaa seuraavat asiat: Kaikkia tietoja ei ole asetettu(nimi, mitat, paino, pakettiluokka, lähtöautomaatti ja määränpään automaatti), nimi sisältää kiellettyjä merkkejä(eli ";", "/" tai "\n", jotka kuuluvat .timo-tiedostojen syntaksiin), mittojen formaatti on väärä(mitat tulee ilmoittaa formaatissa %fx%fx%f, jossa %f tarkoittaa liukulukua tai kokonaislukua), paino on väärää muotoa(painon tulee olla liukuluku tai kokonaisluku) sekä pakettiluokka on valittu väärin tai pakettia ei voida ylipäätään lähettää(se ei sijoitu mihinkään kolmesta luokasta). Jos virhettä ei tule, paketti luodaan ja sen tiedot välittyy Storage-luokalle.

FirstClass.java, SecondClass.java ja ThirdClass.java:
Nämä luokat perivät Package-isäntäluokan tehtävänannon mukaisesti.  Näiden rakentaja kutsuu isäntäluokan rakentajaa(super).

GeoPoint.java:
Jokaisella SmartPost-objektilla on oma GeoPoint-objekti, jossa on tallennettuna automaatin leveys-(latitude) ja pituusasteet(longtitude), eli automaatin koordinaatit. Luokassa on julkinen staattinen metodi, jolla voidaan laskea kahden automaatin etäisyys toisistaan(passaamalla kahden SmartPost-objektin sisältävän ArrayList-muuttujan).

Logger.java:
Tämä luokka toimii kirjanpitäjänä koko ohjelmalle. Se on vastuussa lokitiedoston luomisesta, siihen kirjoittamisesta ja siitä lukemisesta. Tässä noudatetaan myös singleton-periaatetta. Luokka sisältää metodit logIsEmpty(palauttaa totuusarvomuuttujan, onko lokitiedosto tyhjä vai ei; ViewController kutsuu),readFile(lukee lokitiedoston), readLog(prosessoi lokitiedoston sisällön ja kutsuu niillä createClasses-metodia), createClasses(luo paketti-objektit ajonaikaa varten), writeLog(kirjoittaa ohjelman ajon aikana luotujen pakettien tiedot lokitiedostoon, jotta niitä voidaan luoda uudestaan kun ohjelma käynnistetään uudelleen; metodi ajetaan vain juuri ennen ohjelman ajon lopettamista). Logger-luokka sisältää paljon erilaisia try-catchejä, koska tiedoston formaattia on vaikea tarkastaa muuten. Regexillä tähän pystyttäisiin järkevämmin, mutta sitä ei ole ohjelman tekijöille opetettu missään kurssilla.

Main.java:
Pääohjelma, jossa pyörii GUI-ohjelman mahdollistava ikisilmukka. Lisäksi luokka luo ohjelman pääikkunan sekä hallitsee mitä tapahtuu kun ohjelma suljetaan. Main-luokka on yritetty pitää niin minimaalisena kuin mahdollista, jolloin ohjelma pystytään jakamaan mahdollisimman moneen järkevään osioon(eli tiedostoihin, joilla kaikilla on oma selkeä paikkansa ohjelmassa).

Package.java:
Abstrakti luokka jonka jälkeläisinä on aiemmin mainitut FirstClass.java, SecondClass.java ja ThirdClass.java. Sisältää kaikille paketeille vaaditut toiminnallisuudet, eli tiedon tallentamisen luokkamuuttujiin sekä informointi Storage-luokalle itsensä olemassaolostaan. Tämä luokka itsestään tosin poistaa tarpeen kyseisille lapsiluokille.

PackageClassDialogController.java:
PackageClassDialogin ohjain. Käytännössä sisältää vain yhden pitkän merkkijonon ja Sulje-napin toiminnallisuuden.

PackageViewController.java:
PackageViewin ohjain. Rakentaa taulukkonäkymän paketeille.

SmartPost.java:
Kaiki SmartPost-automaatit ovat tämän luokan objekteja. Sisältää luokkamuuttujina automaatin kaikki tarvittavat tiedot. Metodeina lähinnä gettereitä.

Storage.java:
Singleton-objekti, joka pitää kirjaa kaikista luoduista Package-objekteista. Metodeina addPackage(ottaa parametrina Package-objektin, joka lisätään ArrayList-luokkamuuttujaan storedPackages), getStorage(palauttaa storePackages-muuttujan) ja removePackage(poistaa Package-objektin tiedot varastosta kun se lähetetään).

ViewController.java
Pääikkunan ohjain. Hallitsee JavaScript-Java-rajapintaa, ja sisältää ohjelman käytölle tärkeitä komponentteja. 

.timo-tiedostot
Ohjelmaa varten kehitettiin uusi tiedostoformaatti, timo. Tiedostot ovat tällä hetkellä perustekstitiedostoja(ei-binääri), jotka sisältävät Package-objektien tietoja. Jokaisella rivillä on aina kolmen toisiinsa liittyvän objektin tiedot muodossa: 
%s/%s/%s/%s;;%s/%s/%s/%s%s/%s/%s;;%s/%s/%s/%s%s/%s/%s.
Objektit on eroteltu kahdella puolipisteellä. Ensimmäinen objekti on Package(paketti), jonka attribuutteina on  nimi, pakettiluokka, mitat, paino, joista pakettiluokka muutetaan kokonaisluvuksi, mitat muutetaan liukulukutaulukoksi(3) ja paino muutetaan liukuluvuksi. Toinen ja kolmas objekti ovat SmartPost-objekteja, joiden attribuutteina on postinumero, kaupunki, osoite, aukioloajat, toimiston nimi, pituusaste, korkeusaste, joista pituusaste ja korkeusaste muutetaan liukuluvuiksi(postinumeroa ei muuteta kokonaisluvuksi, koska nollalla alkavat postinumerot aiheuttavat häiriötä ohjelmassa, luonnollisesti).


Ohjelman kehitys
Ohjelman loi Esko Heino ja Janne Siivonen, joista molemmat osallistuivat kaikkien osa-alueiden kehitykseen yhtävertaisesti. Ohjelman tekoon käytettiin Eclipse-IDE:ä ja versionhallintana Gittiä. Repository sijaitsee osoitteessa  https://bitbucket.org/jandede/ct60a2411_ht. Ohjelma on testattu Mac- ja Linux-ympäristöissä. Luokkakaavio eroaa hieman ohjeistetusta kaaviosta, rakensimme ohjelman niin, miten näimme loogisimmaksi. Ohjelma on tehty Javan versiolla 8, mutta se on yhteensopiva ainakin version 7 kanssa.

Tavoiteltu vaatimustaso
<<<<<<< HEAD
Ohjelma on pyritty tekemään laajimman vaatimustason mukaisesti. Lähdekoodi on kommentoitu niiltä osin, joissa itse koodi ei välttämättä kerro nopeasti tarkasteltaessa sen toiminnasta. Turhat kommentit on suosiolla otettu pois, ettei kooditiedostot täyty turhalla runoilulla, jota jokainen Javaa kokeillut ymmärtää. Ohjelmassa on kaikki "laajennetun vaatimustason" ominaisuudet, sekä kaksi "laajimman vaatimustason" ominaisuutta(GUI:ssa käytettiin aktiivisesti CSS-tyylitystä, ja standardikomponentteja ei ole JA pelkän logikirjanpidon lisäksi ohjelma lataa kirjanpidon käynnistyessään mikäli käyttäjä haluaa, sekä käyttäjä näkee taulukkonäkymässä tilastot varastotilanteesta).
=======
Ohjelma on pyritty tekemään laajimman vaatimustason mukaisesti. Lähdekoodi on kommentoitu niiltä osin, joissa itse koodi ei välttämättä kerro nopeasti tarkasteltaessa sen toiminnasta. Turhat kommentit on suosiolla otettu pois, ettei kooditiedostot täyty turhalla runoilulla, jota jokainen Javaa kokeillut ymmärtää. Ohjelmassa on kaikki "laajennetun vaatimustason" ominaisuudet, sekä kaksi "laajimman vaatimustason" ominaisuutta(GUI:ssa käytettiin aktiivisesti CSS-tyylitystä, ja standardikomponentteja ei ole JA pelkän logikirjanpidon lisäksi ohjelma lataa kirjanpidon käynnistyessään mikäli käyttäjä haluaa, sekä käyttäjä näkee taulukkonäkymässä tilastot varastotilanteesta).
>>>>>>> 19ad9e6101a4dc7890c94844b5c39e9788d27277
