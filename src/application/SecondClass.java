package application;

/**
* TIMO
* SecondClass.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

import java.util.ArrayList;

public class SecondClass extends Package{
	public SecondClass(String product, double[] dimensions, double weight, ArrayList<SmartPost> startAndDestination) {
		super(product, 2, dimensions, weight, startAndDestination);
	}

}
