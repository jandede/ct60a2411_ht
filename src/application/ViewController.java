package application;
/**
* TIMO
* ViewController.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import com.sun.xml.internal.ws.util.StringUtils;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class ViewController {
	@FXML
	private Button newPacketButton;
	@FXML
	private Button addToMap;
	@FXML
	private Button sendButton;
	@FXML
	private Button updatePackagesButton;
	@FXML
	private Button emptyMapButton;
	@FXML
	private WebView map;
	@FXML
	private ComboBox<String> packetCombo = new ComboBox<String>();
	@FXML
	private ComboBox<String> smartPostCombo = new ComboBox<String>();
	@FXML
	private ArrayList<SmartPost> smartPostList;
	@FXML
	private ArrayList<Package> packages;
	@FXML
	private Storage storage;
	@FXML
	private Button loadLogFileButton;
	@FXML
	private Button showStorageButton;

	// On initialize, open index.html and add all Cities to ComboBox.
	@FXML
	private void initialize() {
		map.getEngine().load(getClass().getResource("index.html").toExternalForm());
		try {
			fillCombo();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Disable this button if no log file exists or if it's empty.
		loadLogFileButton.setDisable(Logger.getInstance().logIsEmpty());
	}

	// Event Listener on Buttons[#newPacketButton].onAction and
	// [#loadLogFileButton].onAction and [#showStorageButton].onAction and
	// [#updatePackagesButton].onAction
	@FXML
	private void handleButtonClick(ActionEvent event) {
		if (event.getSource() == newPacketButton) {
			try {
				newPackageDialog();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (event.getSource() == loadLogFileButton) {
			// Prevent user from loading log file more than once.
			loadLogFileButton.setDisable(true);
			storage.packagesLoadedFromLog = true;
			Logger.getInstance().readLog();
			updatePackageList();
		} else if (event.getSource() == showStorageButton) {
			try {
				showStorage();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (event.getSource() == updatePackagesButton) {
			updatePackageList();
		}
	}

	// Event Listener on Button[#addToMap].onAction
	@FXML
	private void addToMapButton(ActionEvent event) {
		if (!smartPostCombo.getSelectionModel().isEmpty()) {
			for (SmartPost sp : smartPostList) {
				// Make city string on SmartPost object equal capitalized cities on the ComboBox.
				if (StringUtils.capitalize(sp.getCity().toLowerCase()).equals(smartPostCombo.getValue())) {
					map.getEngine()
							.executeScript("document.goToLocation('" + sp.getAddress() + ", " + sp.getCode()
									+ sp.getCity() + "', '" + sp.getPostOffice() + " Aukioloajat: "
									+ sp.getAvailability() + "', 'red')");
				}
			}
		}
	}

	// Event Listener on Button[#sendButton].onAction
	@FXML
	private void send(ActionEvent event) {
		if (!packetCombo.getSelectionModel().isEmpty()) {
			for (Package pkg : packages) {
				if (pkg.product.equals(packetCombo.getValue())) {
					ArrayList<SmartPost> startAndDestSmartPosts = pkg.startAndDestination;
					double startLatAndLng[] = startAndDestSmartPosts.get(0).getLatAndLng();
					double endLatAndLng[] = startAndDestSmartPosts.get(1).getLatAndLng();
					// Change SmartPost coordinates from 2x double array to an ArrayList.
					ArrayList<Double> route = new ArrayList<Double>();
					route.add(startLatAndLng[0]);
					route.add(startLatAndLng[1]);
					route.add(endLatAndLng[0]);
					route.add(endLatAndLng[1]);
					map.getEngine()
							.executeScript("document.createPath(" + route + ", 'red', " + pkg.packageClass + ")");
					storage.removePackage(pkg.product);

					packages = storage.getStorage();
					packetCombo.getItems().clear();
					for (Package pack : packages) {
						packetCombo.getItems().add(pack.product);
					}
					break;
				}
			}
		}
	}

	// Event Listener on Button[#emptyMapButton].onAction
	@FXML
	private void emptyMap(ActionEvent event) {
		map.getEngine().executeScript("document.deletePaths()");
	}

	private void updatePackageList() {
		packages = storage.getStorage();
		packetCombo.getItems().clear();
		for (Package pkg : packages) {
			packetCombo.getItems().add(pkg.product);
		}
	}

	private void fillCombo() throws IOException {
		storage = Storage.getInstance();
		packages = storage.getStorage();
		for (Package pkg : packages) {
			packetCombo.getItems().add(pkg.product);
		}

		DataBuilder db = DataBuilder.getInstance();
		smartPostList = db.getSmartPost();

		ArrayList<String> cities = new ArrayList<String>();

		// Add all cities to ArrayList before adding to ComboBox, remove duplicates.
		for (SmartPost smp : smartPostList) {
			boolean isInList = false;
			for (String c : cities) {
				// Make city string on SmartPost object equal capitalized cities on the ArrayList.
				if (c.equals(StringUtils.capitalize(smp.getCity().toLowerCase()))) {
					isInList = true;
					break;
				}
			}
			if (!isInList) {
				// Make cities first lowercase and then capitalize before adding to ArrayList.
				cities.add(StringUtils.capitalize(smp.getCity().toLowerCase()));
			}
		}
		Collections.sort(cities);
		smartPostCombo.getItems().addAll(cities);
	}

	private void newPackageDialog() throws IOException {
		AnchorPane dialog = (AnchorPane) FXMLLoader.load(getClass().getResource("Dialog.fxml"));
		Scene scene = new Scene(dialog, 370, 325);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}

	private void showStorage() throws IOException {
		AnchorPane dialog = (AnchorPane) FXMLLoader.load(getClass().getResource("PackageView.fxml"));
		Scene scene = new Scene(dialog, 465, 260);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}

}
