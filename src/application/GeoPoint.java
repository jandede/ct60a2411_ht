/**
* TIMO
* GeoPoint.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/
package application;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 *
 */
public class GeoPoint {
	private double lat;
	private double lng;

	public GeoPoint(double latitude, double longitude) {
		lat = latitude;
		lng = longitude;
	}

	/**
	 * @return the lat and lng
	 */
	public double[] getLatAndLng() {
		double[] latLng = {lat, lng};
		return latLng;
	}

	static public double getDistance(ArrayList<SmartPost> sP) {
		double[] latAndLng1 = sP.get(0).getLatAndLng();
		double[] latAndLng2 = sP.get(1).getLatAndLng();

		int earthRadius = 6371;

		double latDistance = Math.toRadians(latAndLng2[0] - latAndLng1[0]);
	    double lonDistance = Math.toRadians(latAndLng2[1] - latAndLng1[1]);
	    double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	            + Math.cos(Math.toRadians(latAndLng1[0])) * Math.cos(Math.toRadians(latAndLng2[0]))
	            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distance = earthRadius * c * 1000; // convert to meters

	    double height = 0 - 0;

	    distance = Math.pow(distance, 2) + Math.pow(height, 2);

	    return  new BigDecimal(Math.sqrt(distance) / 1000).setScale(1, RoundingMode.HALF_UP).doubleValue();
	}

}
