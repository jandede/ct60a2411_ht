package application;

/**
* TIMO
* Package.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

import java.util.ArrayList;

public abstract class Package {
	String product;
	int packageClass;
	double[] dimensions;
	double weight;
	ArrayList<SmartPost> startAndDestination = new ArrayList<SmartPost>();
	public Package(String product, int packageClass, double[] dimensions, double weight,ArrayList<SmartPost>startAndDestination) {
		this.product = product;
		this.packageClass = packageClass;
		this.startAndDestination = startAndDestination;
		this.dimensions = dimensions;
		this.weight = weight;
		Storage.getInstance().addPackage(this);
	}
}
