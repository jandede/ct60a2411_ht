/*
 * Keeps track of log files(reads, writes, creates objects).
/**
* TIMO
* Logger.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/
package application;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Logger {
	static Logger logger = null;
	private String filePath = "./bin/application/log.timo";

	// Create the singleton. Only one instance is needed.
	public static Logger getInstance() {
		if (logger == null)
			logger = new Logger();
		return logger;
	}

	public boolean logIsEmpty() {
		ArrayList<String> log = null;
		try {
			log = readFile(this.filePath);
		} catch (IOException e) {
			// File does not exist.
			return true;
		}
		if(log == null || log.size() == 0)
			return true;
		return false;
	}

	public void readLog() {
		ArrayList<String> log = null;
		try {
			log = readFile(filePath);
		} catch (IOException e) {
			// File does not exist.
			return;
		}
		if (log != null) {
			for (String o : log) {
				String pkg = null, startLocation = null, destinationLocation = null;
				try {
					// As per standard formatting of .timo files. Check documentation.
					pkg = o.split(";;")[0];
					startLocation = o.split(";;")[1];
					destinationLocation = o.split(";;")[2];
				} catch (ArrayIndexOutOfBoundsException aioobe) {
					// Line is corrupted. Ignore it.
					continue;
				}
				createClasses(pkg, startLocation, destinationLocation);
			}
		} else {
			// List is empty. Nothing to do here.
			return;
		}
	}

	// This method will create the objects for the package and two SmartPosts.
	private void createClasses(String pkg, String start, String destination) {
		String[] spStartArguments = start.split("/");
		String[] spDestinationArguments = destination.split("/");
		try {
			// Build objects from log file contents.
			SmartPost spStart = new SmartPost(spStartArguments[0], spStartArguments[1], spStartArguments[2],
					spStartArguments[3], spStartArguments[4], Double.parseDouble(spStartArguments[5]),
					Double.parseDouble(spStartArguments[6]));
			SmartPost spDest = new SmartPost(spDestinationArguments[0], spDestinationArguments[1],
					spDestinationArguments[2], spDestinationArguments[3], spDestinationArguments[4],
					Double.parseDouble(spDestinationArguments[5]), Double.parseDouble(spDestinationArguments[6]));
			ArrayList<SmartPost> startDest = new ArrayList<SmartPost>();
			startDest.add(spStart);
			startDest.add(spDest);

			String[] pkgArguments = pkg.split("/");
			String[] dimensions = pkgArguments[2].split("x");
			double[] dims = null;
			try {
				dims = new double[] { Double.parseDouble(dimensions[0]), Double.parseDouble(dimensions[0]),
						Double.parseDouble(dimensions[0]) };
			} catch (NumberFormatException nfe) {
				System.out.println("Corrupted dimensions");
				return;
			}
			Package p;
			if (pkgArguments[1].equals("1"))
				p = new FirstClass(pkgArguments[0], dims, Double.parseDouble(pkgArguments[3]), startDest);
			else if (pkgArguments[1].equals("2"))
				p = new SecondClass(pkgArguments[0], dims, Double.parseDouble(pkgArguments[3]), startDest);
			else if (pkgArguments[1].equals("3"))
				p = new ThirdClass(pkgArguments[0], dims, Double.parseDouble(pkgArguments[3]), startDest);

		} catch (Exception ex) {
			System.out.println("Corrupted object");
		}
	}

	// This method is executed every time the program closes.
	public void writeLog() {
		// Read file if it exists/if not empty. This is so that we can write the existing contents to the file
		// so it doesn't get overwritten(only relevant if log is NOT loaded during runtime).
		ArrayList<String> existingFile = null;
		try {
			existingFile = readFile(filePath);
		} catch (IOException e) {
			System.out.println("Can't read file in " + filePath);
		}

		String pkg = null, startSP = null, destSP = null;
		Storage storage = Storage.getInstance();
		ArrayList<Package> packages = storage.getStorage();

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(filePath);
			if (existingFile != null && storage.packagesLoadedFromLog == false) {
				for (String line : existingFile) {
					pw.println(line);
				}
			}
		} catch (FileNotFoundException e) {
			// This should not happen because the file will be created if it doesn't exist
			// when readLog() is executed.
			System.exit(1);
		}
		// Update package list as it may be different than last loaded instance.
		// packages = Storage.getInstance().getStorage();
		for (Package p : packages) {
			// double[] to String: [dim1]x[dim2]x[dim3].
			String dims = "";
			try {
				for (double d : p.dimensions) {
					if (!dims.equals(""))
						dims += "x" + d;
					else
						dims += d;
				}
			} catch (NullPointerException npe) {
				System.out.println("Corrupted dimensions in " + filePath);
				continue;
			}
			// pw follows the standard formatting of .timo file specification.
			// "/" separates the arguments.
			// ";;" separates the objects.
			// "\n" separates object groups. Group == Package;;SmartPost;;SmartPost.
			pkg = String.format("%s/%s/%s/%s", p.product, p.packageClass, dims, p.weight);
			SmartPost start = p.startAndDestination.get(0);
			SmartPost dest = p.startAndDestination.get(1);

			startSP = String.format("%s/%s/%s/%s/%s/%s/%s", start.getCode(), start.getCity(), start.getAddress(),
					start.getAvailability(), start.getPostOffice(), start.getLatAndLng()[0], start.getLatAndLng()[1]);
			destSP = String.format("%s/%s/%s/%s/%s/%s/%s", dest.getCode(), dest.getCity(), dest.getAddress(),
					dest.getAvailability(), dest.getPostOffice(), dest.getLatAndLng()[0], dest.getLatAndLng()[1]);
			pw.println(String.format("%s;;%s;;%s", pkg, startSP, destSP));
		}
		pw.close();
	}

	// Returns file contents as ArrayList. One line is one item.
	private ArrayList<String> readFile(String filePath) throws IOException {
		ArrayList<String> objects = new ArrayList<String>();
		BufferedReader reader = null;
		String line = null;
		try {
			reader = new BufferedReader(new FileReader(filePath));
			line = reader.readLine();
		} catch (IOException e) {
			// File does not exist. Nothing to do here.
			return null;
		}
		while (line != null) {
			objects.add(line);
			try {
				line = reader.readLine();
			} catch (IOException e) {
				continue;
			}
		}
		reader.close();

		return objects;
	}

}
