package application;

/**
* TIMO
* DialogController.java
* Esko Heino, Janne Siivonen
* Eclipse Oxygen.1a Release (4.7.1a)
*/

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class DialogController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField massTextField;

	@FXML
	private TextField nameTextField;

	@FXML
	private TextField sizeTextField;

	@FXML
	private ComboBox<String> machineStartCombo = new ComboBox<String>();
	@FXML
	private ComboBox<String> machineDestinationCombo = new ComboBox<String>();
	@FXML
	private ComboBox<String> startCombo = new ComboBox<String>();
	@FXML
	private ComboBox<String> destinationCombo = new ComboBox<String>();

	@FXML
	private Button continueButton;
	@FXML
	private Button cancelButton;

	@FXML
	private ComboBox<?> objectsCombo;

	@FXML
	private CheckBox fragileCheckBox;

	@FXML
	private Button moreInfoClassesButton;

	@FXML
	TextField productTextField;
	@FXML
	MenuButton menuPackageClass;
	@FXML
	ComboBox<String> comboPackageClass = new ComboBox<String>();
	@FXML
	Label errorLabel;
	String[] classes = { "1. luokka", "2. luokka", "3. luokka" };
	DataBuilder db;

	@FXML
	void handleButtonClick(ActionEvent e) {
		if (e.getSource() == moreInfoClassesButton) {
			try {
				packageClassInfo();
			} catch (IOException ioe) {

			}
		}
		// Check if all the necessary information has been entered.
		else if (e.getSource() == continueButton && comboPackageClass.getValue() != null
				&& !nameTextField.getText().equals("") && sizeTextField.getText() != null
				&& massTextField.getText() != null && machineStartCombo.getValue() != null
				&& startCombo.getValue() != null && machineDestinationCombo.getValue() != null
				&& destinationCombo.getValue() != null) {
			errorLabel.setText("");
			String errorMessage = checkInput();
			if (errorMessage != null) {
				errorLabel.setText(errorMessage);
			} else {
				// No exceptions.
				((Node) e.getSource()).getScene().getWindow().hide(); // Closes dialog
			}
		} else if (e.getSource() == continueButton) {
			if (comboPackageClass.getValue() == null) {
				errorLabel.setText("Valitse pakettiluokka.");
			} else if (nameTextField.getText().equals("")) {
				errorLabel.setText("Aseta nimi.");
			} else if (sizeTextField.getText() == null) {
				errorLabel.setText("Aseta mitat.");
			} else if (massTextField.getText() == null) {
				errorLabel.setText("Aseta paino.");
			} else if (machineStartCombo.getValue() == null || startCombo.getValue() == null) {
				errorLabel.setText("Aseta lähtöautomaatti.");
			} else if (machineDestinationCombo.getValue() == null || destinationCombo.getValue() == null) {
				errorLabel.setText("Aseta kohdeautomaatti.");
			}
		} else if (e.getSource() == cancelButton) {
			((Node) e.getSource()).getScene().getWindow().hide(); // Closes dialog.
		}
	}

	// return null == OK, return !null == error code, printed into view. NOT OK.
	private String checkInput() {
		int pkgClass;
		try {
			pkgClass = Integer.parseInt((comboPackageClass.getValue()).substring(0, 1));
		} catch (NullPointerException npe) {
			return "Tarkista luokkavalinta.";
		}
		ArrayList<Double> dim = new ArrayList<Double>();
		try {
			for (String d : sizeTextField.getText().split("x")) {
				try {
					dim.add(Double.parseDouble(d));
				} catch (NumberFormatException nfe) {
					return "Väärät dimensiot.";
				}
			}
			if (dim.size() < 3) {
				return "Väärät dimensiot.";
			}

		} catch (NumberFormatException nfe) {
			return "Väärät dimensiot.";
		}
		double[] dimensions = null;
		try {
			double[] dims = { dim.get(0), dim.get(1), dim.get(2) };
			dimensions = dims;
		} catch (IndexOutOfBoundsException ioobe) {
			return "Väärät dimensiot.";
		}
		String w = massTextField.getText();
		double weight = 0;
		try {
			weight = Double.parseDouble(w);
		} catch (NumberFormatException nfe) {
			return "Tarkista paino.";
		}
		boolean fragile = fragileCheckBox.isSelected();

		// Get the objects that are associated with the selected values.
		// They need to be added to the list in order.
		ArrayList<SmartPost> spList = db.getSmartPost();
		ArrayList<SmartPost> selectedSmartPosts = new ArrayList<SmartPost>();
		for (SmartPost sp : spList) {
			if (sp.getCity().equals(startCombo.getValue()) && sp.getPostOffice().equals(machineStartCombo.getValue())) {
				selectedSmartPosts.add(sp);
			}
		}
		for (SmartPost sp : spList) {
			if (sp.getCity().equals(destinationCombo.getValue())
					&& sp.getPostOffice().equals(machineDestinationCombo.getValue())) {
				selectedSmartPosts.add(sp);
			}
		}
		double distance = 0;
		if (selectedSmartPosts.size() == 2) {
			distance = GeoPoint.getDistance(selectedSmartPosts);
		} else {
			return "Virhe lähtöpaikan/kohteen käsittelyssä.";
		}
		String product = nameTextField.getText();
		if(product.indexOf(";") >= 0 || product.indexOf("/") >= 0 || product.indexOf("\n") >= 0) return "Tuotteen nimessä vääränlaisia merkkejä.";
		return calculatePackageClass(product, dimensions, weight, distance, fragile, selectedSmartPosts, pkgClass);
	}

	/*Decides which class the package belongs to.
	Creates the object.
	Returns:
	String error: displayed to the user
	String null: No problems encountered*/
	private String calculatePackageClass(String product, double[] dimensions, double weight, double distance,
			boolean fragile, ArrayList<SmartPost> selectedSmartPosts, int pkgClass) {
		boolean wrongClassChoice = false;
		if (dimensions[0] <= 19 && dimensions[1] <= 36 && dimensions[2] <= 60 && weight <= 18 && fragile == false
				&& distance >= 150 * 1000) {
			if (pkgClass == 1)
				new FirstClass(product, dimensions, weight, selectedSmartPosts);
			else
				wrongClassChoice = true;
		} else if (dimensions[0] <= 11 && dimensions[1] <= 36 && dimensions[2] <= 60 && weight <= 2) {
			if (pkgClass == 2)
				new SecondClass(product, dimensions, weight, selectedSmartPosts);
			else
				wrongClassChoice = true;
		} else if (dimensions[0] <= 59 && dimensions[1] <= 36 && dimensions[2] <= 60 && weight <= 35
				&& fragile == false) {
			if (pkgClass == 3)
				new ThirdClass(product, dimensions, weight, selectedSmartPosts);
			else
				wrongClassChoice = true;
		} else {
			// No package class exists for this package.
			return "Tätä pakettia ei voida lähettää";
		}
		if(wrongClassChoice)
			return "Väärä pakettiluokkavalinta.";
		return null;
	}

	// Listener for ComboBox. Machine combos are populated when a city is selected.
	@FXML
	private void comboItemSelected(ActionEvent event) {
		if (event.getSource() == startCombo) {
			machineStartCombo.getItems().clear();
			machineStartCombo.getItems().addAll(getMachinesWithinCity(startCombo.getValue()));

		}
		if (event.getSource() == destinationCombo) {
			machineDestinationCombo.getItems().clear();
			machineDestinationCombo.getItems().addAll(getMachinesWithinCity(destinationCombo.getValue()));
		}
	}

	private ArrayList<String> getMachinesWithinCity(String city) {
		ArrayList<String> machines = new ArrayList<String>();
		for (SmartPost sp : db.getSmartPost()) {
			if (sp.getCity() != null && sp.getCity().equals(city)) {
				machines.add(sp.getPostOffice());
			}
		}
		return machines;
	}

	@FXML
	private void initialize() {
		try {
			db = DataBuilder.getInstance();
			ArrayList<SmartPost> sp = db.getSmartPost();
			ArrayList<String> cities = new ArrayList<String>();

			// Add all cities to ArrayList before adding to ComboBox, remove duplicates.
			for (SmartPost smp : sp) {
				boolean isInList = false;
				for (String c : cities)
					if (c.equals(smp.getCity())) {
						isInList = true;
						break;
					}
				if (!isInList)
					cities.add(smp.getCity());
			}
			startCombo.getItems().addAll(cities);
			destinationCombo.getItems().addAll(cities);
			comboPackageClass.getItems().addAll(classes);
		} catch (Exception e) {
		}

	}

	// Opens a static dialog with one label and a close button.
	@FXML
	private void packageClassInfo() throws IOException {
		AnchorPane dialog = (AnchorPane) FXMLLoader.load(getClass().getResource("PackageClassInfoDialog.fxml"));
		Scene scene = new Scene(dialog, 310, 330);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.show();
	}
}
